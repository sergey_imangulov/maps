<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Инструмент для определения координат - API Яндекс.Карт </title>
    <script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <link href="http://yastatic.net/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <style>
      html, body, #map {
        margin: 0;
        padding: 0;
        height: 100%;
      }

      #coord_form{
        position: absolute;
        z-index: 1000;
        background: none repeat scroll 0% 0% rgb(255, 255, 255);
        list-style: none outside none;
        padding: 10px;
        margin: 0px;
        right: 10px;
        top: 50px;
      }
    </style>

    <script type="text/javascript">

    var myMap, myPlacemark, coords;


ymaps.ready(init);


function init () {


    myMap = new ymaps.Map('map', {
        center: [54.3025, 48.3840],
        zoom: 10,
        controls: ['zoomControl', 'typeSelector']
    });

    var searchControl = new ymaps.control.SearchControl({
     options: {
         float: 'left',
         floatIndex: 100,
         noPlacemark: true
     }
});


myMap.controls.add(searchControl);


    coords = [54.3025, 48.3840];

    //Определяем метку и добавляем ее на карту
    myPlacemark = new ymaps.Placemark([54.3025, 48.3840],{}, {preset: "islands#redIcon", draggable: true});

    myMap.geoObjects.add(myPlacemark);


    //Отслеживаем событие перемещения метки
            myPlacemark.events.add("dragend", function (e) {
            coords = this.geometry.getCoordinates();
            savecoordinats();
            }, myPlacemark);

            //Отслеживаем событие щелчка по карте
            myMap.events.add('click', function (e) {
            coords = e.get('coords');
            savecoordinats();
            });



//Отслеживаем событие выбора результата поиска
    searchControl.events.add("resultselect", function (e) {
        coords = searchControl.getResultsArray()[0].geometry.getCoordinates();
        savecoordinats();
    });

    //Ослеживаем событие изменения области просмотра карты - масштаб и центр карты
    myMap.events.add('boundschange', function (event) {
    if (event.get('newZoom') != event.get('oldZoom')) {
        savecoordinats();
    }
      if (event.get('newCenter') != event.get('oldCenter')) {
        savecoordinats();
    }

    });

}

//Функция для передачи полученных значений в форму
    function savecoordinats (){
        var new_coords = [coords[0].toFixed(4), coords[1].toFixed(4)];
        myPlacemark.getOverlaySync().getData().geometry.setCoordinates(new_coords);
        document.getElementById("latlongmet").value = new_coords;
        document.getElementById("mapzoom").value = myMap.getZoom();
        var center = myMap.getCenter();
        var new_center = [center[0].toFixed(4), center[1].toFixed(4)];
        document.getElementById("latlongcenter").value = new_center;
    }
</script>
</head>

<body>
<div id="map"></div>
<div id="coord_form">
<p><label>Координаты метки: </label><input id="latlongmet" class="form-control" name="icon_text" />

<label>Масштаб: </label><input id="mapzoom" class="form-control" name="icon_text" /></p>
<p><label>Центр карты: </label><input id="latlongcenter" class="form-control" name="icon_text" /></p>
</div>

</body>
</html>
